#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	ifstream in("1.txt");
	int E, A, count = 0;
	string line;
	while (getline(in, line))
	{
		E = 0; A = 0;
		for (int j = 0; j < line.length(); j++)
		{
			if (line[j] == 'E') E += 1;
			if (line[j] == 'A') A += 1;
		}
		if (E > A) count += 1;
	}
	cout << "Result: " << count;
}